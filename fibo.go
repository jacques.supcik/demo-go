package demo_go

func Fib (n int) []int {
	if n <= 0 {
		return nil
	} else if (n == 1) {
		return []int{0}
	} else {
		res := []int{0,1}
		for i := 2; i < n; i++ {
			res = append(res, res[len(res)-2] + res[len(res)-1])
		}
		return res
	}
}